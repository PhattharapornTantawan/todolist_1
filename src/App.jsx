import {
	Button,
	Checkbox,
	Container,
	Grid,
	GridItem,
	Input,
	InputGroup,
	Text,
} from "@chakra-ui/react";
import { useState } from "react";
import { EditIcon } from "@chakra-ui/icons";
import { MdDeleteOutline } from "react-icons/md";
import { FaTrashRestore } from "react-icons/fa";
// import theme from './theme';

function App() {
	const [storeItem, setStoreItem] = useState([]);
	const [inputText, setInputText] = useState("");

	const addItem = () => {
		if (inputText.trim() !== "") {
			const newItem = {
				id: Math.random(),
				list: inputText,
				checked: false,
				isDelete: false,
			};
			setStoreItem([...storeItem, newItem]);
			setInputText("");
		}
	};

	const deleteTodo = (id, status) => {
		const updatedList = storeItem.map((item) =>
			item.id === id ? { ...item, isDelete: status } : item
		);
		setStoreItem(updatedList);
		if (status === true && editingTodo === id) {
			setEditingTodo("");
		}
	};

	const handleToggleTodo = (index) => {
		const updatedList = [...storeItem];
		updatedList[index].checked = !updatedList[index].checked;
		setStoreItem(updatedList);
	};

	const sumTodo = () => {
		return storeItem.filter((item) => !item.isDelete).length;
	};

	const sumDeleted = () => {
		return storeItem.filter((item) => item.isDelete).length;
	};

	const [editingTodo, setEditingTodo] = useState("");
	const [editingTodoInput, setEditingTodoInput] = useState("");

	// const iTem = (event) => {
	// 	setInputText(event.target.value);
	// };

	return (
		<Container size='lg' colorScheme='gray'>
			<InputGroup paddingTop='5rem'>
				<Input
					type='text'
					placeholder='What is the task today?'
					_placeholder={{ color: "grey" }}
					height='50px'
					// bgColor='lightgrey'
					value={inputText}
					onChange={(e) => setInputText(e.target.value)}
					// onChange={iTem}
				/>

				<Button
					// bgColor='lightgrey.100'
					width='100px'
					height='50px'
					color='grey'
					onClick={addItem}
					disabled={!inputText.trim()}
				>
					Add Task
				</Button>
			</InputGroup>

			<Grid templateColumns='repeat(2, 1fr)' gap='10' paddingTop='70px'>
				<Grid>
					<Text textAlign='right'>0/{sumTodo()} Todos completed</Text>

					{storeItem
						.filter((item) => !item.isDelete)
						.map((item, index) => (
							<Grid key={item.id}>
								<Grid
									templateColumns='repeat(4, 1fr)'
									borderWidth='30px'
									borderRadius='10px'
									borderColor='white'
									gap={2}
								>
									<GridItem justifyItems='left' key={index}>
										<Checkbox
											isChecked={item.checked}
											onChange={() => handleToggleTodo(index)}
										>
											{editingTodo !== index && (
												<Text width='120px' as={item.checked ? "del" : ""}>
													{item.list}
												</Text>
											)}
										</Checkbox>
									</GridItem>
									{editingTodo === index ? (
										<GridItem>
											<Input
												value={editingTodoInput}
												onChange={(e) => setEditingTodoInput(e.target.value)}
											/>

											<Grid gridTemplateColumns='repeat(3, 1fr)'>
												<GridItem>
													<Button onClick={() => setEditingTodo(false)}>
														Cancel
													</Button>
												</GridItem>
												<GridItem>
													<Button
														onClick={() => {
															setStoreItem((prev) => {
																prev[index].list = editingTodoInput;
																return [...prev];
															});
															setEditingTodo("");
														}}
														isDisabled={editingTodoInput ? false : true}
													>
														Update Task
													</Button>
												</GridItem>
											</Grid>
										</GridItem>
									) : (
										<>
											<GridItem
												paddingLeft='30px'
												onClick={() => {
													setEditingTodoInput(inputText[index]);
													setEditingTodo(index);
												}}
											>
												<EditIcon />
											</GridItem>
											<GridItem
												marginTop='0.5rem'
												color='red'
												paddingLeft='30px'
											>
												<MdDeleteOutline
													onClick={() => deleteTodo(item.id, true)}
												/>
											</GridItem>
										</>
									)}
								</Grid>
							</Grid>
						))}
				</Grid>

				<GridItem key='deleted'>
					<Text textAlign='right'>{sumDeleted()} deleted</Text>

					{storeItem
						.filter((item) => item.isDelete)
						.map((item) => (
							<Grid key={item.id}>
								<Grid
									templateColumns='repeat(3, 1fr)'
									borderWidth='30px'
									borderRadius='10px'
									borderColor='white'
								>
									<Checkbox isChecked={item.checked}>
										<Text width='120px' as={item.checked ? "del" : ""}>
											{item.list}
										</Text>
									</Checkbox>
									<GridItem textAlign='right'>
										<FaTrashRestore
											onClick={() => deleteTodo(item.id, false)}
										/>
									</GridItem>
								</Grid>
							</Grid>
						))}
				</GridItem>
			</Grid>
		</Container>
	);
}

export default App;
